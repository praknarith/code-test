import { Dimensions } from 'react-native'

export default class DeviceUtils {
  static screenWidth = () => Dimensions.get('window').width

  static screenHeight = () => Dimensions.get('window').height
}
