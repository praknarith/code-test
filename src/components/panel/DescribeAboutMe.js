import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

import Colors from '../../constants/Colors'

const DescribeAboutMe = () => (
  <View style={styles.content}>
    <Text style={{ fontSize: 14 }}>3 words that describes a meaningful life to me</Text>
    <Text style={styles.toMe}>FREEDOM • JOY • HEART</Text>
    <Text style={{ fontSize: 11 }}>To be heard, seen and felt.</Text>
  </View>
)

export default DescribeAboutMe

const styles = StyleSheet.create({
  content: {
    flexDirection: 'column',
    backgroundColor: Colors.panel,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginVertical: 20,
    paddingHorizontal: 40,
    paddingVertical: 20
  },
  toMe: {
    fontSize: 19,
    color: Colors.darkBlue,
    marginVertical: 5
  }
})
