import React from 'react'
import { View, StyleSheet } from 'react-native'

import Colors from '../../constants/Colors'
import ImageButton from '../common/ImageButton'
import Icons from '../../constants/Icons'

const Contact = () => (
  <View style={styles.content}>
    <ImageButton image={Icons.call}
      title='Call'
      styleButton={{ marginVertical: 20 }}
    />
    <ImageButton image={Icons.chat}
      title='Chat'
      styleButton={{ marginVertical: 20 }}
    />
    <ImageButton image={Icons.direction}
      title='Direction'
      styleButton={{ marginVertical: 20 }}
    />
    <ImageButton image={Icons.website}
      title='Website'
      styleButton={{ marginVertical: 20 }}
    />
  </View>
)

export default Contact

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    backgroundColor: Colors.panel,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 30
  }
})
