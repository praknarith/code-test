import React from 'react'
import { View, StyleSheet } from 'react-native'

import Colors from '../../constants/Colors'
import ImageButton from '../common/ImageButton'
import Icons from '../../constants/Icons'

const socials = [Icons.fb, Icons.twitter, Icons.instagram, Icons.youtube]
const Social = () => (
  <View style={styles.content}>
    {socials.map((icon, i) => {
      const key = i
      return (
        <ImageButton key={key}
          image={icon}
          styleButton={{ marginVertical: 20 }}
        />
      )
    })}
  </View>
)

export default Social

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    backgroundColor: Colors.panel,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 30
  }
})
