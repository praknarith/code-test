import React from 'react'
import {
  StyleSheet, View, Image, Text
} from 'react-native'
import PropTypes from 'prop-types'

import Icons from '../constants/Icons'
import Colors from '../constants/Colors'

const HeartRating = ({ rating, rateText }) => {
  const rates = []
  for (let i = 1; i <= 5; i += 1) {
    let rate = Icons.love
    if (i > rating) {
      rate = Icons.inactive_love
    }
    rates.push((
      <Image source={rate}
        key={i}
      />
    ))
  }
  return (
    <View style={styles.content}>
      {rates}
      <Text style={styles.text}>{rateText}</Text>
    </View>
  )
}

HeartRating.propTypes = {
  rating: PropTypes.number,
  rateText: PropTypes.string
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: Colors.grey,
    marginLeft: 10
  }
})

export default HeartRating
