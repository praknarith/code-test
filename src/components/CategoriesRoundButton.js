import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import PropTypes from 'prop-types'


import Colors from '../constants/Colors'
import RoundButton from './common/RoundButton'

const CategoriesRoundButton = ({ titles, categoryName }) => (
  <View style={{ marginHorizontal: 40 }}>
    <Text style={styles.title}>{categoryName}</Text>
    <View style={styles.content}>
      {titles.map((title, i) => {
        const key = i
        return (
          <RoundButton key={key}
            title={title}
            toUpperCase
            titleStyle={styles.titleStyle}
            buttonStyle={styles.buttonStyle}
          />
        )
      })}
    </View>
  </View>
)

CategoriesRoundButton.propTypes = {
  titles: PropTypes.array.isRequired,
  categoryName: PropTypes.string
}

export default CategoriesRoundButton

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'space-around',
    flexWrap: 'wrap'
  },
  buttonStyle: {
    borderColor: Colors.darkBlue,
    alignSelf: 'flex-start',
    height: 34,
    margin: 5
  },
  title: {
    fontSize: 17,
    marginBottom: 10
  },
  titleStyle: {
    fontSize: 12,
    marginHorizontal: 10
  }
})
