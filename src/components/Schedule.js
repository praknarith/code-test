import React from 'react'
import {
  View, StyleSheet, Text, FlatList
} from 'react-native'

import Constants from '../constants/Constants'
import DeviceUtils from '../utils/DeviceUtils'
import Colors from '../constants/Colors'

class Schedule extends React.PureComponent {
  renderItem = ({ item }) => (
    <View style={styles.content}>
      <Text>{item.day}</Text>
      <View style={styles.time}>
        <Text>{item.timeIn}</Text>
        <Text>{item.timeOut}</Text>
      </View>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Schedule</Text>
        <FlatList data={Constants.schedule}
          renderItem={this.renderItem}
          keyExtractor={(_, index) => index.toString()}
        />
        <Text style={styles.download}>Download PDF</Text>
      </View>
    )
  }
}

export default Schedule

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 40,
    marginVertical: 20
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5
  },
  time: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: DeviceUtils.screenWidth() / 3
  },
  title: {
    fontSize: 17,
    marginBottom: 10
  },
  download: {
    color: Colors.lightRed,
    marginTop: 10
  }
})
