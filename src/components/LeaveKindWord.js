import React from 'react'
import {
  View, StyleSheet, Text, FlatList
} from 'react-native'

import Colors from '../constants/Colors'

import RoundButton from './common/RoundButton'
import Comment from './common/Comment'
import Constants from '../constants/Constants'

class LeaveKindWord extends React.PureComponent {
  renderItem = ({ item, index }) => (
    <View>
      {index === 6 ? <Text style={styles.more}>+ More Kind Words</Text> : null}
      <Comment rating={item.rating}
        since={item.since}
        comment={item.comment}
        displayName={item.displayName}
      />
      {
        item.isChild ? (
          <Comment rating={item.rating}
            since={item.since}
            comment={item.comment}
            displayName={item.displayName}
            isChild
          />
        ) : null
      }
      {index === 0 ? null : <View style={styles.separator} />}
    </View>
  )

  render() {
    return (
      <View>
        <View style={styles.textView}>
          <Text style={styles.title}>Kind Words</Text>
          <Text style={{ color: Colors.grey }}>(5)</Text>
        </View>
        <RoundButton title='Leave Kind Words'
          titleStyle={{ color: 'white' }}
          buttonStyle={styles.contactMe}
        />
        <FlatList data={comments}
          renderItem={this.renderItem}
          keyExtractor={(_, index) => index.toString()}
        />
      </View>
    )
  }
}

export default LeaveKindWord

const styles = StyleSheet.create({
  textView: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: 40
  },
  contactMe: {
    backgroundColor: Colors.orange,
    borderColor: Colors.panel,
    height: 54,
    marginVertical: 10,
    marginHorizontal: 40
  },
  title: {
    fontSize: 17,
    marginRight: 5
  },
  separator: {
    height: 1,
    position: 'absolute',
    width: '100%',
    backgroundColor: Colors.lightGrey
  },
  more: {
    color: Colors.lightRed,
    alignSelf: 'center',
    marginVertical: 10
  }
})

const comments = [
  {
    rating: 4, since: '4 weeks ago', comment: Constants.comment, displayName: 'Tanya'
  },
  {
    rating: 5, since: '3 weeks ago', comment: Constants.comment, displayName: 'Loga'
  },
  {
    rating: 2, since: '3 weeks ago', comment: Constants.comment, displayName: 'Tanya'
  },
  {
    rating: 2, since: '3 weeks ago', comment: Constants.comment, displayName: 'Tanya'
  },
  {
    rating: 5, since: '1 weeks ago', comment: Constants.comment, displayName: 'Lanka', isChild: true
  },
  {
    rating: 2, since: '3 weeks ago', comment: Constants.comment, displayName: 'Tanya'
  },
  {
    rating: 4, since: '4 weeks ago', comment: Constants.comment, displayName: 'Tanya'
  },
  {
    rating: 4, since: '4 weeks ago', comment: Constants.comment, displayName: 'Tanya'
  }
]
