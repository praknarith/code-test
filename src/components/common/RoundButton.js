import React from 'react'
import {
  StyleSheet, TouchableOpacity, Text, ViewPropTypes
} from 'react-native'
import PropTypes from 'prop-types'

const RoundButton = ({
  title, titleStyle, buttonStyle, toUpperCase
}) => (
  <TouchableOpacity style={[buttonStyle, styles.content]}>
    <Text style={titleStyle}>{toUpperCase ? title.toUpperCase() : title}</Text>
  </TouchableOpacity>
)

RoundButton.propTypes = {
  title: PropTypes.string.isRequired,
  titleStyle: PropTypes.object,
  buttonStyle: ViewPropTypes.style,
  toUpperCase: PropTypes.bool
}

export default RoundButton

const styles = StyleSheet.create({
  content: {
    borderRadius: 34,
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
