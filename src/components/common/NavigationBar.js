import React from 'react'
import {
  StyleSheet, View, Image, Text, TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'

import Colors from '../../constants/Colors'
import Icons from '../../constants/Icons'

const NavigationBar = ({ backTitle, leftButtonImage }) => (
  <View style={styles.container}>
    <View style={styles.navBack}>
      <TouchableOpacity>
        <Image style={{ marginRight: 20 }}
          source={Icons.back}
        />
      </TouchableOpacity>
      <Text>{backTitle}</Text>
    </View>
    <TouchableOpacity>
      <Image style={{ marginRight: 20 }}
        source={leftButtonImage}
      />
    </TouchableOpacity>
  </View>
)

NavigationBar.propTypes = {
  backTitle: PropTypes.string,
  leftButtonImage: Image.propTypes.source
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.lightGrey,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60
  },
  navBack: {
    flexDirection: 'row',
    alignContent: 'flex-start',
    marginLeft: 20
  }
})

export default NavigationBar
