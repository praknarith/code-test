import React from 'react'
import {
  View, Text, StyleSheet, Image
} from 'react-native'
import PropTypes from 'prop-types'

import HeartRating from '../HeartRating'
import Colors from '../../constants/Colors'
import Images from '../../constants/Images'

const Comment = ({
  rating, since, comment, displayName, isChild
}) => (
  <View style={styles.content}>
    {
      isChild ? (
        <View style={styles.child}>
          <Image source={Images.photo}
            resizeMode='cover'
            style={styles.image}
          />
          <View style={styles.text}>
            <Text style={styles.since}>{since}</Text>
            <Text style={styles.comment}>{comment}</Text>
            <Text style={{ color: Colors.yellow }}>{displayName}</Text>
          </View>
          <Text style={styles.edit}>Edit</Text>
        </View>
      )
        : (
          <View>
            <HeartRating rating={rating}
              rateText={since}
            />
            <Text style={{ marginVertical: 10 }}>{comment}</Text>
            <Text style={{ color: Colors.yellow }}>{displayName}</Text>
          </View>
        )
    }
  </View>
)

Comment.propTypes = {
  rating: PropTypes.number,
  since: PropTypes.string,
  comment: PropTypes.string,
  displayName: PropTypes.string,
  isChild: PropTypes.bool
}

export default Comment

const styles = StyleSheet.create({
  content: {
    marginVertical: 10,
    marginHorizontal: 40
  },
  image: {
    borderColor: Colors.lightGrey,
    borderWidth: 2,
    height: 50,
    width: 50,
    borderRadius: 25
  },
  text: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  child: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  since: {
    color: Colors.grey,
    marginBottom: 10
  },
  edit: {
    position: 'absolute',
    right: 20,
    color: Colors.darkBlue,
    textDecorationLine: 'underline'
  },
  comment: {
    marginRight: 40,
    marginVertical: 10
  }
})
