import React from 'react'
import {
  TouchableOpacity, Image, StyleSheet, Text, ViewPropTypes
} from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../../constants/Colors'

const ImageButton = ({ image, title, styleButton }) => (
  <TouchableOpacity style={[styles.button, styleButton]}>
    <Image source={image} />
    {title ? (<Text style={styles.title}>{title.toUpperCase()}</Text>) : null}
  </TouchableOpacity>
)

ImageButton.propTypes = {
  image: Image.propTypes.source,
  title: PropTypes.string,
  styleButton: ViewPropTypes.style
}

export default ImageButton

const styles = StyleSheet.create({
  button: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: Colors.grey,
    fontSize: 11,
    marginTop: 10
  }
})
