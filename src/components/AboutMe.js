import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

import Constants from '../constants/Constants'

const AboutMe = () => (
  <View style={{ marginHorizontal: 40 }}>
    <Text style={styles.title}>About</Text>
    <Text style={{ fontSize: 12 }}>{Constants.about}</Text>
  </View>
)

export default AboutMe

const styles = StyleSheet.create({
  title: {
    fontSize: 17,
    marginVertical: 10
  }
})
