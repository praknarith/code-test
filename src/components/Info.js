import React from 'react'
import {
  View, Image, StyleSheet, Text, TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'

import Colors from '../constants/Colors'
import Icons from '../constants/Icons'
import DeviceUtils from '../utils/DeviceUtils'

const Info = ({ profilePhoto, name, position }) => (
  <View style={styles.content}>
    <Image source={profilePhoto}
      resizeMode='cover'
      style={styles.profilePhoto}
    />
    <View style={styles.text}>
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.position}
        numberOfLines={2}
      >
        {position}
      </Text>
    </View>
    <View style={styles.button}>
      <TouchableOpacity>
        <Image source={Icons.share} />
      </TouchableOpacity>
      <TouchableOpacity style={{ marginLeft: 10 }}>
        <Image source={Icons.fav} />
      </TouchableOpacity>
    </View>
  </View>
)

Info.propTypes = {
  profilePhoto: Image.propTypes.source,
  name: PropTypes.string,
  position: PropTypes.string
}

export default Info

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    marginVertical: 30
  },
  profilePhoto: {
    borderColor: Colors.lightGrey,
    borderWidth: 2,
    height: 79,
    width: 79,
    borderRadius: 79 / 2
  },
  name: {
    fontSize: 17,
    color: Colors.lightRed,
    marginBottom: 5
  },
  position: {
    fontSize: 14,
    color: Colors.grey,
    width: DeviceUtils.screenWidth() / 2
  },
  text: {
    flexDirection: 'column',
    justifyContent: 'center',
    margin: 10
  },
  button: {
    flexDirection: 'row',
    position: 'absolute',
    right: 20
  }
})
