import React from 'react'
import {
  Image, View, StyleSheet, ScrollView, Animated
} from 'react-native'

import Images from '../constants/Images'
import Colors from '../constants/Colors'
import DeviceUtils from '../utils/DeviceUtils'

const images = [Images.undersea, Images.sea, Images.bali, Images.forest]
class SwipeableImage extends React.PureComponent {
  scrollX = new Animated.Value(0)

  renderScrollImage = () => (
    <View style={styles.content}>
      <ScrollView horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: this.scrollX } } }]
        )}
        scrollEventThrottle={16}
      >
        {images.map((source, i) => {
          const key = i
          return (
            <Image key={key}
              style={styles.content}
              source={source}
            />
          )
        })}
      </ScrollView>
    </View>
  )

  renderIndicator = () => {
    const position = Animated.divide(this.scrollX, DeviceUtils.screenWidth())
    return (
      <View style={styles.dotContent}>
        {images.map((_, i) => {
          const backgroundColor = position.interpolate({
            inputRange: [i - 1, i, i + 1],
            outputRange: ['white', Colors.yellow, 'white'],
            extrapolate: 'clamp'
          })
          const key = i
          return (
            <Animated.View key={key}
              style={[{ backgroundColor }, styles.dot]}
            />
          )
        })}
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderScrollImage()}
        {this.renderIndicator()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    width: DeviceUtils.screenWidth() - 20,
    height: 210
  },
  dotContent: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 20
  },
  dot: {
    height: 10,
    width: 10,
    margin: 4,
    borderRadius: 5
  }
})

export default SwipeableImage
