import React from 'react'
import {
  StyleSheet, View, Text, ScrollView
} from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'

import Icons from '../constants/Icons'
import Colors from '../constants/Colors'
import Images from '../constants/Images'
import DeviceUtils from '../utils/DeviceUtils'
import Constants from '../constants/Constants'

import NavigationBar from '../components/common/NavigationBar'
import HeartRating from '../components/HeartRating'
import SwipeableImage from '../components/SwipeableImage'
import Info from '../components/Info'
import RoundButton from '../components/common/RoundButton'
import Contact from '../components/panel/Contact'
import AboutMe from '../components/AboutMe'
import DescribeAboutMe from '../components/panel/DescribeAboutMe'
import CategoriesRoundButton from '../components/CategoriesRoundButton'
import Schedule from '../components/Schedule'
import Social from '../components/panel/Social'
import LeaveKindWord from '../components/LeaveKindWord'

const HomeScreen = () => (
  <View style={styles.container}>
    <NavigationBar backTitle='Access Bar'
      leftButtonImage={Icons.filter}
    />
    <ScrollView>
      <View style={styles.content}>
        <View style={styles.topHeader}>
          <Text style={styles.header}>Soul Voice</Text>
          <HeartRating rating={4}
            rateText='(137)'
          />
        </View>
        <SwipeableImage />
        <Info profilePhoto={Images.photo}
          name='Trish Watts'
          position='Voice Movement Therapy Practitioner'
        />
        <RoundButton title='Contact Me'
          titleStyle={{ color: 'white' }}
          buttonStyle={styles.contactMe}
        />
      </View>
      <Contact />
      {Maps()}
      <CategoriesRoundButton titles={Constants.Categories}
        categoryName='Categories'
      />
      <AboutMe />
      <DescribeAboutMe />
      <CategoriesRoundButton titles={Constants.Symptoms}
        categoryName='Symptoms'
      />
      <Schedule />
      <Social />
      <LeaveKindWord />
    </ScrollView>
  </View>
)

const Maps = () => (
  <View style={styles.mapView}>
    <Text style={{ marginBottom: 10 }}>LOCATION: PADDINGTON</Text>
    <MapView style={styles.map}
      provider={PROVIDER_GOOGLE}
      showsMyLocationButton
      region={{
        latitude: 11.5238982,
        longitude: 104.9300443,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121
      }}
    />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  header: {
    color: Colors.lightRed,
    fontSize: 18,
    fontWeight: '400'
  },
  content: {
    marginHorizontal: 40
  },
  topHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 10
  },
  contactMe: {
    backgroundColor: Colors.lightRed,
    borderColor: Colors.panel,
    height: 54
  },
  mapView: {
    marginHorizontal: 40,
    justifyContent: 'center'
  },
  map: {
    width: DeviceUtils.screenWidth() - 80,
    height: 200,
    marginBottom: 20
  }
})

export default HomeScreen
