/* eslint-disable global-require */
const Images = {
  undersea: require('../assets/images/undersea.png'),
  bali: require('../assets/images/bali.jpg'),
  forest: require('../assets/images/forest.jpg'),
  sea: require('../assets/images/sea.jpg'),
  photo: require('../assets/images/soul_adviser.png')
}

export default Images
