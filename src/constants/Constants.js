/* eslint-disable max-len */
const Constants = {
  about: 'I am a singer, songwriter, and educator with extensive experience as a performing artist & facilitator in Australia and overseas. \n\nI am a registered Voice Movement Therapy practitioner, Accredited Singing Teacher with a Diploma of Music Education. \n\nPlay is the bedrock of my life, co-founding InterPlay Australia & Cambodia Sings!',
  comment: '“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.”',
  Categories: ['BREATHWORK', 'SOUND HEALING', 'INTUITIVE BUSINESS COACHING'],
  Symptoms: [
    'ANXIETY', 'creativity issue', 'confidence issue', 'headaches',
    'energy problem', 'relaxation issue', 'self-awareness issue',
    'stress management', 'vitality'
  ],
  schedule: [
    { day: 'Monday', timeIn: '01:30', timeOut: '03:30' },
    { day: 'Tuesday', timeIn: '00:30', timeOut: '02:00' },
    { day: 'Wednesday', timeIn: '01:00', timeOut: '03:00' },
    { day: 'Thursday', timeIn: '01:00', timeOut: '02:30' },
    { day: 'Friday', timeIn: '01:00', timeOut: '03:00' },
    { day: 'Saturday', timeIn: '01:00', timeOut: '03:00' },
    { day: 'Sunday', timeIn: '01:30', timeOut: '03:30' }
  ]
}

export default Constants
