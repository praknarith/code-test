/* eslint-disable global-require */
const Icons = {
  // App icons
  back: require('../assets/icons/app/back/back.png'),
  call: require('../assets/icons/app/call/call.png'),
  chat: require('../assets/icons/app/chat/chat.png'),
  direction: require('../assets/icons/app/direction/direction.png'),
  fav: require('../assets/icons/app/fav/fav.png'),
  filter: require('../assets/icons/app/filter/filter.png'),
  inactive_love: require('../assets/icons/app/inactive_love/inactive_love.png'),
  love: require('../assets/icons/app/love/love.png'),
  share: require('../assets/icons/app/share/share.png'),
  website: require('../assets/icons/app/website/website.png'),

  // Social icons
  fb: require('../assets/icons/social/fb/fb.png'),
  instagram: require('../assets/icons/social/instagram/instagram.png'),
  twitter: require('../assets/icons/social/twitter/twitter.png'),
  youtube: require('../assets/icons/social/youtube/youtube.png')
}

export default Icons
