/* eslint-disable global-require */
const Colors = {
  lightGrey: '#EFEFF4',
  grey: '#747474',
  lightRed: '#F15E69',
  yellow: '#EFAC3D',
  panel: '#E5EAEF',
  darkBlue: '#006276',
  orange: '#EB6E1F'
}

export default Colors
